require 'rails_helper'

RSpec.describe DriversController do
  render_views

  before do
    key = ApiKey.create
    request.env['HTTP_AUTHORIZATION'] =
      ActionController::HttpAuthentication::Token.encode_credentials(key.auth_token)
  end

  describe 'GET #status' do
    it 'returns driver status data' do
      create(:driver, id: 1, latitude: -23.612474, longitude: -46.702746, available: true)
      get :status, driver_id: 1

      parsed_response = JSON.parse(response.body, symbolize_names: true)

      expect(response.status).to eq(200)
      expect(parsed_response).to eq({
          driverId: 1,
          driverAvailable: true,
          latitude: -23.612474,
          longitude: -46.702746
        })
    end

    it 'returns status 404 when driver id does not exist' do
      get :status, driver_id: 1

      expect(response.status).to eq(404)
    end
  end

  describe 'POST #update_status' do
    it 'updates the driver status' do
      create(:driver, id: 1, latitude: -23.612474, longitude: -46.702746, available: false)
      post :update_status, driver_id: 1,
        driverId: 1,
        driverAvailable: true,
        latitude: 40.6892534,
        longitude: -74.0466891

      expect(response.status).to eq(200)

      driver = Driver.find(1)
      expect(driver.available).to be_truthy
      expect(driver.latitude).to eq(40.6892534)
      expect(driver.longitude).to eq(-74.0466891)
    end

    it 'returns status 409 if driverId does not match the route id' do
      create(:driver, id: 1, latitude: -23.612474, longitude: -46.702746, available: false)
      post :update_status, driver_id: 1, driverId: 2

      expect(response.status).to eq(409)
    end

    it 'returns status 404 when driver does not exist' do
      post :update_status, driver_id: 1

      expect(response.status).to eq(404)
    end
  end

  describe 'GET #in_area' do
    it 'returns available drivers who matches the given rectangle' do
      create(:driver, id: 1, latitude: 1, longitude: 5, available: true)
      create(:driver, id: 2, latitude: 2, longitude: 10, available: true)
      create(:driver, id: 3, latitude: 3, longitude: 15, available: false)

      get :in_area, sw: '2,10', ne: '4,20'

      parsed_response = JSON.parse(response.body, symbolize_names: true)

      expect(parsed_response.size).to eq(1)
      expect(parsed_response.first[:driverId]).to eq(2)
    end

    it 'returns 400 status if ne is not on north east from sw' do
      get :in_area, ne: '0,0', sw: '1,1'

      expect(response.status).to eq(400)
    end
  end

  context 'not authenticated' do
    it 'return status 401' do
      request.env['HTTP_AUTHORIZATION'] = nil
      get :in_area, sw: '0,0', ne: '1,1'

      expect(response.status).to eq(401)
    end
  end
end
