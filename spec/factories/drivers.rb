FactoryGirl.define do
  factory :driver do
    name 'Travis Bickle'
    sequence(:car_plate) { |n| 'XXX-%04d' % n }
  end
end
