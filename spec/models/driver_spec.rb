require 'rails_helper'

RSpec.describe Driver do
  context 'validations' do
    subject { create(:driver) }

    it { is_expected.to validate_presence_of :name }
    it { is_expected.to validate_presence_of :car_plate }
    it { is_expected.to validate_uniqueness_of(:car_plate) }
    it { is_expected.to allow_value('ABC-1234').for(:car_plate) }
    it { is_expected.not_to allow_value('ABC1234').for(:car_plate) }
    it { is_expected.not_to allow_value('abc-1234').for(:car_plate) }
    it { is_expected.not_to allow_value('abc-12345').for(:car_plate) }
  end

  context 'scopes' do
    describe '#available' do
      it 'only returns available drivers' do
        d1 = create(:driver, available: true)
        d2 = create(:driver, available: false)

        drivers = Driver.available

        expect(drivers.size).to eq(1)
        expect(drivers.first.id).to eq(d1.id)
      end
    end

    describe '#in_rectangle' do
      it 'returns drivers who matches the given rectangle (sw, ne)' do
        create(:driver, id: 1, latitude: 1, longitude: 5, available: true)
        create(:driver, id: 2, latitude: 2, longitude: 10, available: true)
        create(:driver, id: 3, latitude: 3, longitude: 15, available: false)
        create(:driver, id: 4, latitude: 4, longitude: 20, available: false)

        drivers = Driver.in_rectangle(GeoPoint.new(2,10), GeoPoint.new(3,15))

        expect(drivers.map(&:id)).to match_array([2,3])
      end
    end
  end
end
