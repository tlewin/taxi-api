require 'rails_helper'

RSpec.describe ApiKey do
  context 'validations' do
    it { is_expected.to validate_presence_of(:auth_token) }
    it { is_expected.to validate_uniqueness_of(:auth_token) }
  end
end
