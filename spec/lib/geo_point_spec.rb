require 'rails_helper'

RSpec.describe GeoPoint do
  describe '.from_string' do
    it 'returns a GeoPoint from string' do
      geo_point = GeoPoint.from_string('-23.612474,-46.702746')

      expect(geo_point).to be_an_instance_of(GeoPoint)
      expect(geo_point.latitude).to eq(-23.612474)
      expect(geo_point.longitude).to eq(-46.702746)
    end
  end

  describe '#north_east?' do
    it 'returns true if the point is located north east from a given point' do
      ne = GeoPoint.new(-23.589548,-46.673392)
      sw = GeoPoint.new(-23.612474,-46.702746)

      expect(ne.north_east?(sw)).to be_truthy
      expect(sw.north_east?(ne)).to be_falsey
    end
  end
end
