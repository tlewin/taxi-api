# Taxi-api

Backend de um aplicativo de smartphone que mostra um mapa com os taxistas ativos.

## Instalação

O aplicativo foi desenvolvido usando rails + postgresql.

Para rodar a api, é necessário ter uma instância do postgresql iniciada.

Para criar o banco de dados:

```
> rake db:setup
```

Isso deve gerar um token (`token`) e 1.000 motoristas uniformemente distribuídos no intervalo: lat: -23.62..-23.60, lng: -46.72..-46.70

## Inicialização

Para iniciar o rails:

```
> rails s
```

## Requests

Para fazer requests é necessário incluir o token de autenticação no cabeçalho. Exemplo:

```
curl -H "Authorization: Token token=token" \
  'http://localhost:3000/drivers/inArea?sw=-23.615,-46.702746&ne=-23.61-46.673392'
```

Para fazer um POST é necessário incluir o cabeçalho `Content-type: application/json`. Exemplo:

```
curl -XPOST -H "Content-type: application/json" \
  -H "Authorization: Token token=token" \
  http://localhost:3000/drivers/1/status \
  -d '{"latitude":-23.60810717,"longitude":-46.67500346,"driverId":1,"driverAvailable":true}'
```

## Autenticação

Para fazer a autenticação dos endpoints foi utilizado um esquema de verificação de tokens, onde cada request deve incluir o token de autenticação no cabeçalho.

O token é verificado na tabela `ApiKeys` e caso haja o registro, o processamento da API segue normalmente. Do contrário, a API retorna o status 401 (Unauthorized).

Tal abordagem se mostrou bem flexível, pois facilmente podemos incluir um sistema de role adicionando uma coluna `role` na tabela `ApiKeys`.

Outra possibilidade é associar a tabela `ApiKeys` a tabela de `Drivers`, permitindo tokens individuais por motoristas.

E ainda podemos associar um mecanismo de login/ senha para motoristas e usuários junto do modelo de autenticação por tokens, pois esses dois mecanimos podem ser tratados individualmente.

## Busca por taxistas

Para a busca de taxista, precisamos do ponto de localzação do usuário (x, y) e uma distância máxima D.

Podemos assumir que D é pequeno suficiente para utilizarmos a fórmula de distância euclidiana sem ter impacto na precisão, pois o ganho utilizando a fórmula de Haversine, nesse caso, é muito baixo comparado ao esforço computacional.

Com isso, conseguimos definir dois pontos:

```
sw = (x - D, y - D) # southwest
ne = (x + D, y + D) # northeast
```

Esse pontos definem um quadrado que sobrepõe a circunferência de raio D.

Sendo assim, podemos utilizar o nosso endpoint `/drivers/inArea`.

Eventualmente, essa busca trará motoristas que se encontram fora do raio D, pois a área do quadrado é maior que a da circunferência, mas tal imprecisão não trás impacto algum ao usuário final e performa significativamente melhor que busca limitada por uma circunferência.


## Specs

Para rodar as specs do projeto basta rodar o seguinte comando na raíz do projeto:

```
> rspec
```
