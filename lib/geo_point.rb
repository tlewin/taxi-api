class GeoPoint
  attr_accessor :latitude, :longitude

  alias :lat :latitude
  alias :lng :longitude

  def initialize(latitude, longitude)
    @latitude  = latitude
    @longitude = longitude
  end

  def north_east?(geo_point)
    latitude > geo_point.latitude &&
      longitude > geo_point.longitude
  end

  def self.from_string(string)
    lat, lng = string.to_s.split(',').map(&:to_f)
    new(lat || 0.0, lng || 0.0)
  end
end
