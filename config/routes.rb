Rails.application.routes.draw do
  get  'drivers/:driver_id/status'  => 'drivers#status',        as: :driver_status
  post 'drivers/:driver_id/status'  => 'drivers#update_status', as: :update_driver_status
  get  'drivers/inArea'             => 'drivers#in_area',       as: :drivers_in_area
  post 'drivers'                    => 'drivers#create',        as: :drivers_create
end
