class CreateApiKeys < ActiveRecord::Migration
  def change
    create_table :api_keys do |t|
      t.string :auth_token, null: false
    end

    add_index :api_keys, :auth_token, unique: true
  end
end
