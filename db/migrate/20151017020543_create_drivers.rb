class CreateDrivers < ActiveRecord::Migration
  def change
    create_table :drivers do |t|
      t.string  :name,       null: false
      t.string  :car_plate,  null: false
      t.float   :latitude
      t.float   :longitude
      t.boolean :available,  default: false
    end

    add_index :drivers, :car_plate, unique: true
    add_index :drivers, [:latitude, :longitude],
      where: 'available IS TRUE'
  end
end
