# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)
key = ApiKey.new
key.auth_token = 'token'
key.save

# Create some random drivers
# lat: -23.62..-23.60
# lng: -46.72..-46.70
1000.times do |i|
  Driver.create({
    id: i + 1,
    name: "Sample #{i}",
    car_plate: 'ANY-%04d' % i,
    latitude: -23.62 + (rand.round(5)/50),
    longitude: -46.72 + (rand.round(5)/50),
    available: rand >= 0.7
  })
end
