class ApplicationController < ActionController::Base
  rescue_from ActiveRecord::RecordNotFound, with: :not_found

  before_filter :force_json_format
  before_filter :authenticate!

  def not_found
    render json: { errors: ['record not found'] }, status: :not_found
  end

  private

  def force_json_format
    request.format = :json
  end

  def authenticate!
    unless authenticate_token
      render json: { errors: ['invalid token'] }, status: :unauthorized
    end
  end

  def authenticate_token
    authenticate_with_http_token do |token, options|
      ApiKey.find_by(auth_token: token)
    end
  end
end
