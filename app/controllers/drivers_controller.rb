class DriversController < ApplicationController
  def status
    @driver = Driver.find(params[:driver_id])
  end

  def update_status
    driver = Driver.find(params[:driver_id])

    if params[:driver_id] != params[:driverId].to_s
      render json: { errors: ["driverId doesn't match to route driver's id."]}, status: :conflict
    elsif driver.update_attributes(status_params)
      render json: { status: :ok }
    else
      render json: { errors: driver.errors.full_messages }, status: :bad_request
    end
  end

  def in_area
    sw = GeoPoint.from_string(params[:sw])
    ne = GeoPoint.from_string(params[:ne])

    if ne.north_east?(sw)
      @drivers = Driver.available.in_rectangle(sw, ne)
    else
      render json: { errors: ['ne is not on north east from sw'] }, status: :bad_request
    end
  end

  def create
    driver = Driver.create(create_params)

    if driver.persisted?
      render json: { status: :ok }
    else
      render json: { errors: driver.errors.full_messages }, status: :bad_request
    end
  end

  private

  def status_params
    # Rename available attribute
    if params.has_key?(:driverAvailable)
      params.merge!(available: params[:driverAvailable])
    end

    params.permit(:available, :latitude, :longitude)
  end

  def create_params
    if params.has_key?(:carPlate)
      params.merge!(car_plate: params[:carPlate])
    end

    params.permit(:name, :car_plate)
  end
end
