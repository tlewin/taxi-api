json.key_format! camelize: :lower

json.driver_id driver.id
json.driver_available driver.available
json.(driver, :latitude, :longitude)
