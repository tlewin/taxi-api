class ApiKey < ActiveRecord::Base
  validates :auth_token, presence: true, uniqueness: true

  after_initialize :reset_auth_token, if: :new_record?

  def reset_auth_token
    self.auth_token = SecureRandom.urlsafe_base64(24)
  end
end
