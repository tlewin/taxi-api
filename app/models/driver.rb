# == Schema Information
#
# Table name: drivers
#
#  id        :integer          not null, primary key
#  name      :string           not null
#  car_plate :string           not null
#  latitude  :float
#  longitude :float
#  available :boolean          default(FALSE)
#
# Indexes
#
#  index_drivers_on_car_plate               (car_plate) UNIQUE
#  index_drivers_on_latitude_and_longitude  (latitude,longitude)
#

class Driver < ActiveRecord::Base
  validates_presence_of :name, :car_plate
  validates_format_of :car_plate, with: /\A[A-Z]{3}-\d{4}\z/
  validates_uniqueness_of :car_plate

  scope :available, -> { where('available IS TRUE') }

  scope :in_rectangle, -> (sw, ne) do
    where(latitude: (sw.lat..ne.lat), longitude: (sw.lng..ne.lng))
  end
end
